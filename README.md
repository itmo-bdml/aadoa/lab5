## Lab 5

### Algorithms on graphs. Introduction to graphs and basic algorithms on graphs

---
[Report (google doc)](https://docs.google.com/document/d/1atQVv_ZQLTxOgRvOQNzYtk3jJyyrwwxOpCXItBLaYQ4)

Performed by:
* Elya Avdeeva
* Gleb Mikloshevich

### part one

adjacency matrix for the 0th vertex: [0. 0. 0. 0. 1. 0. 0. 1. 0. 0. 1. 0. 0. 0. 0. 0. 0. 0. 1. 0. 0. 0. 0. 0. 0. 0. 1. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 1. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.]

adjacency matrix for the 1st vertex: [0. 0. 1. 0. 0. 1. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 1. 0. 0. 0. 0. 0. 1. 1. 0. 0. 0. 1. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 1. 0. 0. 0. 1. 0. 1. 0. 1. 0. 0. 0. 0. 0. 0. 0. 0.]

adjacency list for the 0th vertex: [4, 7, 10, 18, 26, 47]

adjacency list for the 1st vertex: [2, 5, 52, 58, 59, 63, 83, 87, 89, 91]

**Graph Visualisation**

<img src="./lab/plots/graph.png" height="350">

---

### part two

**depth-first search**

1. Number of clusters: 2
2. Number of elements in each cluster: {0: 99, 1: 1}

The graph has only two clusters. One cluster with 99 vertices and another cluster - an isolated vertex.


**breadth-first search**

We took two random vertices from the graph and found the shortest path between them using the BFS algorithm.

Starting vertex: 5, finish vertex: 57

path length: 4 edges

path: [5 -> 1 -> 52 -> 21 -> 57]

<img src="./lab/plots/path_on_graph.png" height="350">

Visualisation of path. Yep, you can barely see the path :)

### part three

We used different data structures:
1. Graph
2. List
3. Queue
4. Stack
