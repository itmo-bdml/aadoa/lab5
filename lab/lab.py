import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import networkx as nx

matplotlib.use('TkAgg')

np.random.seed(42)
VERTICES: int = 100
EDGES: int = 200

adjacency_matrix = np.zeros((VERTICES, VERTICES))

# generating an adjacency matrix
i = 0
while i < EDGES:
    v1, v2 = np.random.randint(low=0, high=VERTICES, size=2)
    if v1 == v2 or adjacency_matrix[v1, v2]:
        continue
    adjacency_matrix[v1, v2] = 1
    adjacency_matrix[v2, v1] = 1
    i += 1

# converting adjacency matrix to adjacency list
adjacency_list = [[] for i in range(VERTICES)]

for i, row in enumerate(adjacency_matrix):
    for j, elem in enumerate(row):
        if adjacency_matrix[i][j]:
            adjacency_list[i].append(j)

# Task 1
print(f"---Part 1---")

print(f"adjacency matrix for the 0th vertex: {adjacency_matrix[0]}")
print(f"adjacency matrix for the 1st vertex: {adjacency_matrix[1]}", end="\n\n")

print(f"adjacency list for the 0th vertex: {adjacency_list[0]}")
print(f"adjacency list for the 1st vertex: {adjacency_list[1]}")

G = nx.from_numpy_array(adjacency_matrix)
pos = nx.drawing.spring_layout(G)

plt.figure(figsize=(8, 6))
nx.draw_networkx(G, font_size=8, pos=pos)
plt.savefig("./plots/graph.png")
# plt.show()


# Task 2
print(f"\n---Part 2---")


def dfs(adjacency_list: list[list[int]]) -> list:
    visited = [False] * len(adjacency_list)
    clusters = [-1] * len(adjacency_list)
    cur_cluster = -1
    for i in range(len(adjacency_list)):
        if visited[i]:
            continue
        cur_cluster += 1
        to_visit = [i]
        while len(to_visit):
            cur_vertex = to_visit.pop()
            for vertex in adjacency_list[cur_vertex]:
                if not visited[vertex]:
                    to_visit.append(vertex)
            visited[cur_vertex] = True
            clusters[cur_vertex] = cur_cluster

    return clusters


clusters = dfs(adjacency_list)
print(f"Number of clusters: {np.max(clusters)+1}")
unique, counts = np.unique(clusters, return_counts=True)
print(f"Number of elements in each cluster: {dict(zip(unique, counts))}")


def bfs(adjacency_list: list[list[int]], start: int, target: int) -> tuple[int, list]:
    visited = [False] * len(adjacency_list)
    distances = [-1] * len(adjacency_list)
    distances[start] = 0
    to_visit = [start]
    path = []

    flag = False
    while len(to_visit):
        cur_vertex = to_visit.pop(0)
        for vertex in adjacency_list[cur_vertex]:
            if not visited[vertex]:
                to_visit.append(vertex)
                distances[vertex] = distances[cur_vertex] + 1
            visited[cur_vertex] = True
            if cur_vertex == target:
                flag = True
                break
        if flag:
            break
    else:
        return -1, []

    # generating the path
    path.append(target)
    cur_distance = distances[target]
    cur_path_vertex = target
    while cur_distance > 0:
        for v in adjacency_list[cur_path_vertex]:
            if distances[v] == cur_distance-1:
                cur_distance -= 1
                cur_path_vertex = v
                path.append(v)
                break
    return distances[target], list(reversed(path))


start, finish = np.random.randint(low=0, high=VERTICES, size=2)
print(f"start: {start}, finish: {finish}")
path_len, path = bfs(adjacency_list, start=start, target=finish)
print(f"path_len: {path_len}")
print(f"path: {path}")

edge_list = [(path[i], path[i+1])for i in range(len(path)-1)]


plt.figure(figsize=(8, 6))
nx.draw_networkx(G, pos=pos, font_size=8, alpha=0.5, style="--")
nx.draw_networkx_edges(G.subgraph(path), pos=pos, edgelist=edge_list, width=1.5, edge_color='red')
nx.draw_networkx(G.subgraph(path), pos=pos, font_size=8, node_color='red')
plt.savefig("./plots/path_on_graph.png")
plt.show()

